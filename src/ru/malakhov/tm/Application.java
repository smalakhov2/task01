package ru.malakhov.tm;

import ru.malakhov.tm.cons.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("** Welcome to task manager.**");
        parseArg(args);
    }

    private static void parseArg (String[] args){
        if (args.length == 0) return;
        final String arg  = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
    }

    private static void showHelp(){
        System.out.println("[Help]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + "- Show version info.");
        System.out.println(TerminalConst.HELP + " - display terminal command");
    }

    private static void showAbout(){
        System.out.println("[About]");
        System.out.println("Name: Sergei Malakhov");
        System.out.println("Email: smalakhov2@rencredit.ru");
    }

    private static void showVersion(){
        System.out.println("[Version]");
        System.out.println("1.0.0");
    }

}
