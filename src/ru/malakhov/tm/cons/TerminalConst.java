package ru.malakhov.tm.cons;

public interface TerminalConst {
    String HELP = "help";
    String ABOUT = "about";
    String VERSION = "version";
}
